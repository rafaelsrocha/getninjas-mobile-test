//
//  DetailsViewController.swift
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit

class DetailsViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    var job: Job!
    var fromAcceptedOffer = false
    private var data: NSDictionary?
    @IBOutlet var tableView: UITableView!
    @IBOutlet var leftBottomLabel: UILabel!
    @IBOutlet var leftBottomButton: UIButton!
    @IBOutlet var verticalDivider: UIView!
    @IBOutlet var rightBottomLabel: UILabel!
    @IBOutlet var rightBottomButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = job!.title
        tableView.registerNib(UINib(nibName: "DetailMapCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "DetailMapCell")
        tableView.registerNib(UINib(nibName: "DetailHolderCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "DetailHolderCell")
        tableView.registerNib(UINib(nibName: "DetailInfoCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "DetailInfoCell")
        tableView.registerNib(UINib(nibName: "DetailHighlightedCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "DetailHighlightedCell")
        tableView.registerNib(UINib(nibName: "DetailFooterCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "DetailFooterCell")
        
        tableView.contentInset = UIEdgeInsetsMake(16, 0, 70, 0)
        
        if job.isLead() || fromAcceptedOffer {
            leftBottomLabel.text = "LIGAR"
            rightBottomLabel.text = "WHATSAPP"
            leftBottomLabel.textColor = UIColor(red: 25/255, green: 146/255, blue: 251/255, alpha: 1)
            rightBottomLabel.textColor = UIColor(red: 25/255, green: 146/255, blue: 251/255, alpha: 1)
            verticalDivider.hidden = false
            leftBottomButton.backgroundColor = UIColor.clearColor()
            rightBottomLabel.backgroundColor = UIColor.clearColor()
            leftBottomButton.addTarget(self, action: #selector(call), forControlEvents: .TouchUpInside)
            rightBottomButton.addTarget(self, action: #selector(openWhatsapp), forControlEvents: .TouchUpInside)
        } else {
            leftBottomLabel.text = "RECUSAR"
            rightBottomLabel.text = "ACEITAR"
            leftBottomLabel.textColor = UIColor(red: 123/255, green: 123/255, blue: 123/255, alpha: 1)
            rightBottomLabel.textColor = UIColor.blackColor()
            verticalDivider.hidden = true
            leftBottomButton.backgroundColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
            rightBottomLabel.backgroundColor = UIColor.greenColor()
            leftBottomButton.addTarget(self, action: #selector(decline), forControlEvents: .TouchUpInside)
            rightBottomButton.addTarget(self, action: #selector(accept), forControlEvents: .TouchUpInside)
        }
        
        showLoading()
        
        let url = NSURL(string: job!.url)!
        let request = NSMutableURLRequest(URL:url)
        let queue = NSOperationQueue()
        
        NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler:{ (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            if error != nil {
                print("error: \(error!.localizedDescription)")
            } else {
                self.data = try! NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as? NSDictionary
                self.dismissHUD()
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    // MARK: - Data
    
    func embedded() -> NSDictionary {
        return data!["_embedded"] as! NSDictionary
    }
    
    func info() -> NSArray {
        return embedded()["info"] as! NSArray
    }
    
    func user() -> NSDictionary {
        return embedded()["user"] as! NSDictionary
    }
    
    func address() -> NSDictionary {
        return embedded()["address"] as! NSDictionary
    }
    
    // MARK: - Table View
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if data == nil || data!.allValues.count <= 0 {
            return 0
        }
        
        return info().count + 4 // +4 = Map, User Info, User Contact and Footer
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("DetailMapCell", forIndexPath: indexPath) as! DetailMapCell
            cell.titleLabel.text = data!["title"] as? String
            cell.setMapLocation((embedded()["address"] as! NSDictionary)["geolocation"] as! NSDictionary)
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("DetailHolderCell", forIndexPath: indexPath) as! DetailHolderCell
            cell.userLabel.text = user()["name"] as? String
            cell.locationLabel.text = "\(address()["neighborhood"] as! String) - \(address()["city"] as! String)"
            let distance = (data!["distance"] as! Double) / 1000
            cell.distanceLabel.text = "a \(String(format: "%.2f", distance)) Km de você"
            return cell
        } else if indexPath.row == info().count + 2 {
            let cell = tableView.dequeueReusableCellWithIdentifier("DetailHighlightedCell", forIndexPath: indexPath) as! DetailHighlightedCell
            cell.emailLabel.text = user()["email"] as? String
            
            let phones = NSMutableArray()
            for item in (user()["_embedded"] as! NSDictionary)["phones"] as! NSArray {
                let dict = item as! NSDictionary
                phones.addObject(dict["number"] as! String)
            }
            cell.phoneLabel.text = phones.componentsJoinedByString(", ")
            
            let labelColor = job.isLead() || fromAcceptedOffer ? UIColor.blackColor() : UIColor(red: 225/255, green: 243/255, blue: 253/255, alpha: 1)
            let backgroundColor = job.isLead() || fromAcceptedOffer ? UIColor(red: 201/255, green: 243/255, blue: 132/255, alpha: 1) :
                UIColor(red: 85/255, green: 199/255, blue: 252/255, alpha: 1)
            
            cell.contentView.backgroundColor = backgroundColor
            cell.titleLabel.textColor = labelColor
            cell.phoneLabel.textColor = labelColor
            cell.emailLabel.textColor = labelColor
            
            return cell
        } else if indexPath.row == info().count + 3 {
            let cell = tableView.dequeueReusableCellWithIdentifier("DetailFooterCell", forIndexPath: indexPath) as! DetailFooterCell
            if job.isLead() || fromAcceptedOffer {
                cell.label.text = "Fale com o cliente o quanto antes"
            } else {
                cell.label.text = "Aceite o pedido para destravar o contato!"
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("DetailInfoCell", forIndexPath: indexPath) as! DetailInfoCell
        
        let currentInfo = info()[indexPath.row - 2] as! NSDictionary
        cell.label.text = currentInfo["label"] as? String
        if let arrayInfo = currentInfo["value"] as? NSArray {
            cell.valueLabel.text = arrayInfo.componentsJoinedByString(", ")
        } else {
            cell.valueLabel.text = currentInfo["value"] as? String
            if cell.valueLabel.text == "N/A" {
                cell.valueLabel.text = ""
            }
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 225
        } else if indexPath.row == 1 {
            return 140
        } else if indexPath.row == info().count + 2 {
            return 150
        } else if indexPath.row == info().count + 3 {
            return 100
        }
        
        return 80
    }
    
    // MARK: - Buttons actions
    
    func accept() {
        let vc = storyboard?.instantiateViewControllerWithIdentifier("DetailsViewController") as! DetailsViewController
        vc.fromAcceptedOffer = true
        
        job.url = ((data!["_links"] as! NSDictionary)["accept"] as! NSDictionary)["href"] as! String
        vc.job = job
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func decline() {
        if let vc = navigationController!.viewControllers[navigationController!.viewControllers.count - 2] as? MainViewController {
            vc.shouldReload = true
            navigationController?.popToViewController(vc, animated: true)
        } else {
            navigationController?.popViewControllerAnimated(true)
        }
    }
    
    func call() {
        let phones = (user()["_embedded"] as! NSDictionary)["phones"] as! NSArray
        if phones.count < 2 {
            let dict = phones.firstObject as! NSDictionary
            let phone = dict["number"] as! String
            let cleanString = phone.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet).joinWithSeparator("")
            let url = NSURL(string: "tel:\(cleanString)")!
            
            if UIApplication.sharedApplication().canOpenURL(url) {
                UIApplication.sharedApplication().openURL(url)
            }
        } else {
            let actionSheet = UIAlertController(title: user()["name"] as? String, message: "Selecione um número para ligar", preferredStyle: .ActionSheet)
            for item in phones {
                let phone = (item as! NSDictionary)["number"] as! String
                actionSheet.addAction(UIAlertAction(title: phone, style: .Default, handler: { (alert: UIAlertAction!) -> Void in
                    let cleanString = phone.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet).joinWithSeparator("")
                    let url = NSURL(string: "tel:\(cleanString)")!
                    
                    if UIApplication.sharedApplication().canOpenURL(url) {
                        UIApplication.sharedApplication().openURL(url)
                    }
                }))
            }
            actionSheet.addAction(UIAlertAction(title: "Cancelar", style: .Cancel, handler: nil))
            presentViewController(actionSheet, animated: true, completion: nil)
        }
    }
    
    func openWhatsapp() {
        let url = NSURL(string: "whatsapp://send?")
        
        if UIApplication.sharedApplication().canOpenURL(url!) {
            UIApplication.sharedApplication().openURL(url!)
        }
    }
}
