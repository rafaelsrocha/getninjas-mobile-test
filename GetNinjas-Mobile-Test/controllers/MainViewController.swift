//
//  MainViewController.swift
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/1/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController, RequestManagerDelegate, UITableViewDataSource, UITableViewDelegate {
    
    var shouldReload = false
    private var offers: NSArray?
    private var leads: NSArray?
    @IBOutlet var tableView: UITableView!
    @IBOutlet var jobSegmentControl: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(UINib(nibName: "JobCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "JobCell")
        
        showLoading()
        
        let rm = RequestManager.sharedManager()
        rm.delegate = self
        rm.getOffers()
        
        tableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if shouldReload {
            refreshControl.beginRefreshing()
            tableView.setContentOffset(CGPointMake(0, -refreshControl.frame.size.height), animated: true)
            refresh()
            shouldReload = false
        }
    }
    
    func finishedRequest(items: [AnyObject]!) {
        if jobSegmentControl.selectedSegmentIndex == 0 {
            offers = items
        } else if jobSegmentControl.selectedSegmentIndex == 1 {
            leads = items
        }
        
        refreshControl.endRefreshing()
        dismissHUD()
        tableView.reloadData()
    }
    
    func failedRequest(error: NSError!) {
        refreshControl.endRefreshing()
        dismissHUD()
        showError(error.localizedDescription)
    }
    
    @IBAction func jobListSelection(sender: AnyObject) {
        if jobSegmentControl.selectedSegmentIndex == 0 {
            if offers == nil {
                showLoading()
                let rm = RequestManager.sharedManager()
                rm.delegate = self
                rm.getOffers()
            } else {
                tableView.reloadData()
            }
        } else if jobSegmentControl.selectedSegmentIndex == 1 {
            if leads == nil {
                showLoading()
                let rm = RequestManager.sharedManager()
                rm.delegate = self
                rm.getLeads()
            } else {
                tableView.reloadData()
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let verificationArray = jobSegmentControl.selectedSegmentIndex == 0 ? offers : leads
        if verificationArray == nil || verificationArray?.count == 0 {
            let label: UILabel = UILabel(frame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height))
            label.text = "No \(jobSegmentControl.selectedSegmentIndex == 0 ? "offers" : "leads") to show. 😕"
            label.textColor = UIColor.whiteColor()
            label.numberOfLines = 0
            label.textAlignment = NSTextAlignment.Center
            label.font = UIFont.systemFontOfSize(16.0)
            label.sizeToFit()
            tableView.backgroundView = label
            
            return 0
        }
        
        tableView.backgroundView = nil
        return verificationArray!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("JobCell", forIndexPath: indexPath) as! JobCell
        
        let job = (jobSegmentControl.selectedSegmentIndex == 0 ? offers![indexPath.row] : leads![indexPath.row]) as! Job
        
        cell.titleLabel.text = job.title
        cell.userLabel.text = job.user.name
        cell.locationLabel.text = "\(job.address.neighborhood) - \(job.address.city)"
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd MMM"
        cell.dateLabel.text = formatter.stringFromDate(job.createdAt)
        
        if jobSegmentControl.selectedSegmentIndex == 0 {
            cell.statusIndicator.hidden = false
            cell.statusIndicator.backgroundColor = job.state == "unread" ? UIColor.blueColor() : UIColor.lightGrayColor()
        } else { cell.statusIndicator.hidden = true }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let job = (jobSegmentControl.selectedSegmentIndex == 0 ? offers![indexPath.row] : leads![indexPath.row]) as! Job
        performSegueWithIdentifier("DetailSegue", sender: job)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let dest = segue.destinationViewController as! DetailsViewController
        dest.job = sender as! Job
    }
    
    // MARK: - Refresh
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.whiteColor()
        refreshControl.addTarget(self, action: #selector(refresh), forControlEvents: .ValueChanged)
        
        return refreshControl
    }()
    
    func refresh() {
        if jobSegmentControl.selectedSegmentIndex == 0 {
            let rm = RequestManager.sharedManager()
            rm.delegate = self
            rm.refreshOffersList()
        } else if jobSegmentControl.selectedSegmentIndex == 1 {
            let rm = RequestManager.sharedManager()
            rm.delegate = self
            rm.refreshLeadsList()
        }
    }
}
