//
//  BaseViewController.swift
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/1/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()];
        navigationController?.navigationBar.barTintColor = UIColor(red: 25/255.0, green: 152/255.0, blue: 242/255.0, alpha: 1.0)
        navigationController?.navigationBar.translucent = false
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        let gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [
            UIColor(red: 32/255.0, green: 187/255.0, blue: 252/255.0, alpha: 1.0).CGColor,
            UIColor(red: 24/255.0, green: 144/255.0, blue: 251/255.0, alpha: 1.0).CGColor
        ]
        
        view.layer.insertSublayer(gradient, atIndex: 0)
    }
    
    func showLoading() {
        dispatch_async(dispatch_get_main_queue()) {
            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.Black)
            SVProgressHUD.show()
        }
    }
    
    func showError(error: String) {
        dispatch_async(dispatch_get_main_queue()) {
            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.Black)
            SVProgressHUD.showErrorWithStatus(error)
        }
    }
    
    func dismissHUD() {
        dispatch_async(dispatch_get_main_queue()) {
            SVProgressHUD.dismiss()
        }
    }
}
