//
//  Job.m
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/2/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import "Job.h"

@implementation Job

- (BOOL)isLead {
    return self.user.email != nil && ![self.user.email isEqualToString:@""];
}

@end
