//
//  User.h
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import "BaseEntity.h"

@interface User : BaseEntity
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *email;
@end
