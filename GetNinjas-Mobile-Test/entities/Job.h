//
//  Job.h
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/2/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import "BaseEntity.h"
#import "User.h"
#import "Address.h"

@interface Job : BaseEntity
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSDate *createdAt;
@property (strong, nonatomic) User *user;
@property (strong, nonatomic) Address *address;
@property (strong, nonatomic) NSString *url;

@property (strong, nonatomic) NSString *state;
- (BOOL)isLead;
@end
