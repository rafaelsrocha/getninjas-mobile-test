//
//  Address.h
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import "BaseEntity.h"

@interface Address : BaseEntity
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *street;
@property (strong, nonatomic) NSString *neighborhood;
@property (strong, nonatomic) NSString *uf;
@end
