//
//  BaseEntity.h
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/2/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseEntity : NSObject

@end
