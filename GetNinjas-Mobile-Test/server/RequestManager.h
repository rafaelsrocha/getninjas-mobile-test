//
//  RequestManager.h
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/1/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RequestManagerDelegate <NSObject>

- (void)finishedRequest:(NSArray *)items;
- (void)failedRequest:(NSError *)error;

@end

@interface RequestManager : NSObject

@property (assign, nonatomic) id<RequestManagerDelegate> delegate;

+ (instancetype)sharedManager;
- (void)getOffers;
- (void)getLeads;
- (void)refreshOffersList;
- (void)refreshLeadsList;

@end
