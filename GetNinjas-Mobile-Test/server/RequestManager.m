//
//  RequestManager.m
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/1/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

#import <RestKit/RestKit.h>
#import "RequestManager.h"
#import "Job.h"
#import "GetNinjas-Swift.h"

@class AppDelegate;

@interface RequestManager ()
@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) RKObjectManager *objectManager;
@property (strong, nonatomic) NSString *refreshOffers;
@property (strong, nonatomic) NSString *refreshLeads;
@end

@implementation RequestManager

#pragma mark - Initializers

+ (instancetype)sharedManager {
    static RequestManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [RequestManager new];
        sharedManager.objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:[sharedManager.appDelegate baseUrl]]];
        [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"binary/octet-stream"];
    });
    
    return sharedManager;
}

- (AppDelegate *)appDelegate {
    return ((AppDelegate *)[UIApplication sharedApplication].delegate);
}

#pragma mark - Requests

- (void)getOffers {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[self.appDelegate baseUrl]]];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
   [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
       NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
       NSString *offersUrl = responseDict[@"_links"][@"offers"][@"href"];
       NSString *http = [[self.appDelegate baseUrl] stringByReplacingOccurrencesOfString:@"https" withString:@"http"];
       [self offersRequest:[offersUrl stringByReplacingOccurrencesOfString:http withString:@""]];
   }];
}

- (void)offersRequest:(NSString *)path {
    NSIndexSet *successCode = [NSIndexSet indexSetWithIndex:200];
    RKResponseDescriptor *offerDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[self offerMapping]
                                                                                         method:RKRequestMethodGET
                                                                                    pathPattern:path
                                                                                        keyPath:@"offers"
                                                                                    statusCodes:successCode];
    [self.objectManager addResponseDescriptor:offerDescriptor];
    
    [self.objectManager getObjectsAtPath:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        RKLogInfo(@"Success w/ %lu objects", (unsigned long)result.array.count);
        
        NSError *error;
        NSDictionary *dict = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:operation.HTTPRequestOperation.responseData options:kNilOptions error: &error];
        self.refreshOffers = dict[@"_links"][@"self"][@"href"];
        
        [self.delegate finishedRequest:result.array];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        RKLogError(@"Failed with error: %@", [error localizedDescription]);
        [self.delegate failedRequest:error];
    }];
}

- (void)getLeads {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[self.appDelegate baseUrl]]];
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSString *offersUrl = responseDict[@"_links"][@"leads"][@"href"];
        NSString *http = [[self.appDelegate baseUrl] stringByReplacingOccurrencesOfString:@"https" withString:@"http"];
        [self leadsRequest:[offersUrl stringByReplacingOccurrencesOfString:http withString:@""]];
    }];
}

- (void)leadsRequest:(NSString *)path {
    NSIndexSet *successCode = [NSIndexSet indexSetWithIndex:200];
    RKResponseDescriptor *leadDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[self leadMapping]
                                                                                        method:RKRequestMethodGET
                                                                                   pathPattern:path
                                                                                       keyPath:@"leads"
                                                                                   statusCodes:successCode];
    [self.objectManager addResponseDescriptor:leadDescriptor];
    
    [self.objectManager getObjectsAtPath:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        RKLogInfo(@"Success w/ %lu objects", (unsigned long)result.array.count);
        
        NSError *error;
        NSDictionary *dict = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:operation.HTTPRequestOperation.responseData options:kNilOptions error: &error];
        self.refreshLeads = dict[@"_links"][@"self"][@"href"];
        
        [self.delegate finishedRequest:result.array];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        RKLogError(@"Failed with error: %@", [error localizedDescription]);
        [self.delegate failedRequest:error];
    }];
}

- (void)refreshOffersList {
    [self offersRequest:[self.refreshOffers stringByReplacingOccurrencesOfString:[self.appDelegate baseUrl] withString:@""]];
}

- (void)refreshLeadsList {
    [self leadsRequest:[self.refreshLeads stringByReplacingOccurrencesOfString:[self.appDelegate baseUrl] withString:@""]];
}

#pragma mark - Mappings

- (RKObjectMapping *)offerMapping {
    RKObjectMapping *offerMapping = [RKObjectMapping mappingForClass:Job.class];
    [offerMapping addAttributeMappingsFromDictionary:@{
                                                       @"state" : @"state",
                                                       @"_embedded.request.title" : @"title",
                                                       @"_embedded.request.created_at" : @"createdAt",
                                                       @"_links.self.href" : @"url"
                                                     }];
    [offerMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"_embedded.request._embedded.user"
                                                                                 toKeyPath:@"user"
                                                                               withMapping:[self userMapping]]];
    [offerMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"_embedded.request._embedded.address"
                                                                                 toKeyPath:@"address"
                                                                               withMapping:[self addressMapping]]];
    
    return offerMapping;
}

- (RKObjectMapping *)leadMapping {
    RKObjectMapping *leadMapping = [RKObjectMapping mappingForClass:Job.class];
    [leadMapping addAttributeMappingsFromDictionary:@{
                                                      @"created_at" : @"createdAt",
                                                      @"_embedded.request.title" : @"title",
                                                      @"_links.self.href" : @"url"
                                                    }];
    [leadMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"_embedded.user"
                                                                                toKeyPath:@"user"
                                                                              withMapping:[self userMapping]]];
    [leadMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"_embedded.address"
                                                                                toKeyPath:@"address"
                                                                              withMapping:[self addressMapping]]];
    return leadMapping;
}

- (RKObjectMapping *)userMapping {
    RKObjectMapping *userMapping = [RKObjectMapping mappingForClass:User.class];
    [userMapping addAttributeMappingsFromArray:@[@"name", @"email"]];
    return userMapping;
}

- (RKObjectMapping *)addressMapping {
    RKObjectMapping *addressMapping = [RKObjectMapping mappingForClass:Address.class];
    [addressMapping addAttributeMappingsFromArray:@[@"city", @"neighborhood", @"uf", @"street"]];
    return addressMapping;
}

@end
