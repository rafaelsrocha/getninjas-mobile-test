//
//  DetailHighlightedCell.swift
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/4/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit

class DetailHighlightedCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
}
