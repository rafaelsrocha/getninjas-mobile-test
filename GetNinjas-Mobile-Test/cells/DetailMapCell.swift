//
//  DetailMapCell.swift
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit
import MapKit

class DetailMapCell: UITableViewCell, MKMapViewDelegate {

    @IBOutlet var mapView: MKMapView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var divider: IPDashedLineView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        divider.lineColor = UIColor(white: 0.8, alpha: 1)
        divider.lengthPattern = [6, 3]
    }
    
    func setMapLocation(geolocation: NSDictionary) {
        if let lat = geolocation["latitude"] as? Double, let lng = geolocation["longitude"] as? Double {
            let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
            let region = MKCoordinateRegion(center: coordinate, span: span)
            mapView.setRegion(region, animated: true)
            
            let circle = MKCircle(centerCoordinate: coordinate, radius: 400)
            mapView.addOverlay(circle)
        }
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        let circleView = MKCircleRenderer(overlay: overlay)
        circleView.fillColor = UIColor(red: 32/255.0, green: 187/255.0, blue: 252/255.0, alpha: 1.0).colorWithAlphaComponent(0.4)
        return circleView
    }
}
