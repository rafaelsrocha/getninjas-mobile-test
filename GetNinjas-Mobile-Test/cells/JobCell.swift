//
//  JobCell.swift
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/1/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit

class JobCell: UITableViewCell {

    @IBOutlet var roundedBackground: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var dashedLine: IPDashedLineView!
    @IBOutlet var userLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var statusIndicator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        statusIndicator.layer.cornerRadius = 10
        statusIndicator.clipsToBounds = true
        roundedBackground.layer.cornerRadius = 10
        roundedBackground.clipsToBounds = true
        dashedLine.lineColor = UIColor(white: 0.8, alpha: 1)
        dashedLine.lengthPattern = [6, 3]
    }
}
