//
//  DetailInfoCell.swift
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit

class DetailInfoCell: UITableViewCell {

    @IBOutlet var label: UILabel!
    @IBOutlet var valueLabel: UILabel!
}
