//
//  DetailFooterCell.swift
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/4/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit

class DetailFooterCell: UITableViewCell {

    @IBOutlet var label: UILabel!
    @IBOutlet var bottomRoundedView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bottomRoundedView.layer.cornerRadius = 20
        bottomRoundedView.clipsToBounds = true
        
        let fontDescriptor = label.font.fontDescriptor().fontDescriptorWithSymbolicTraits(UIFontDescriptorSymbolicTraits(arrayLiteral: .TraitBold, .TraitItalic))
        label.font = UIFont(descriptor: fontDescriptor, size: 0)
    }
}
