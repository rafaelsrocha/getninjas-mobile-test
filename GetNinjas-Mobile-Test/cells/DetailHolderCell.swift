//
//  DetailHolderCell.swift
//  GetNinjas-Mobile-Test
//
//  Created by Rafael Rocha on 5/3/16.
//  Copyright © 2016 Rocha, O Desenvolvedor. All rights reserved.
//

import UIKit

class DetailHolderCell: UITableViewCell {

    @IBOutlet var userLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var divider: IPDashedLineView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        divider.lineColor = UIColor(white: 0.8, alpha: 1)
        divider.lengthPattern = [6, 3]
    }
}
